﻿using Employees.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees.Domain.Interfaces
{
    public interface IEmployeeRepository: IDisposable
    {
        IQueryable<Employee> GetEmployees();
        Task<Employee> GetEmployeeAsync(int id);
        Task CreateEmployeeAsync(Employee item);
        Task UpdateEmployeeAsync(Employee item);
        Task DeleteEmployeeAsync(int id);
        Task SaveAsync();
    }
}
