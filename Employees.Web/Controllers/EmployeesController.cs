﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Employees.Domain.Core;
using Employees.Infrastructure.Data;
using Employees.Domain.Interfaces;

namespace Employees.Web.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IEmployeeRepository db;
        public EmployeesController(IEmployeeRepository db)
        {
            this.db = db;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Grid()
        {
            return PartialView(await db.GetEmployees().ToListAsync());
        }


        public ActionResult Create()
        {
            ViewBag.Chiefs = GetChiefsSelectList();

            return PartialView();
        }

        [HttpPost]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Position,ChiefId")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                await db.CreateEmployeeAsync(employee);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            ViewBag.Chiefs = GetChiefsSelectList();

            return PartialView(employee);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await db.GetEmployeeAsync(id.GetValueOrDefault());
            if (employee == null)
            {
                return HttpNotFound();
            }

            ViewBag.Chiefs = GetChiefsSelectList(id.Value);


            return PartialView(employee);
        }


        [HttpPost]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Position,ChiefId")] Employee employee)
        {
            var chiefs = GetChiefsSelectList();

            if (ModelState.IsValid)
            {
                await db.UpdateEmployeeAsync(employee);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return PartialView(employee);
        }


        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await db.GetEmployeeAsync(id.GetValueOrDefault());
            if (employee == null)
            {
                return HttpNotFound();
            }
            return PartialView(employee);
        }


        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await db.DeleteEmployeeAsync(id);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        private SelectList GetChiefsSelectList(int idExcept = 0)
        {
            var employees = db.GetEmployees().AsNoTracking().Where(e => e.Id != idExcept).ToList();
            return new SelectList(employees, "Id", "Name");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
