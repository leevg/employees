﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Employees.Domain.Core;
using Employees.Domain.Interfaces;


namespace Employees.Infrastructure.Data
{
    public class EmployeeRepository : IEmployeeRepository, IDisposable
    {
        private EmployeesContext db;

        public EmployeeRepository()
        {
            this.db = new EmployeesContext();
        }
        public async Task CreateEmployeeAsync(Employee item)
        {
            db.Employees.Add(item);
            await SaveAsync();
        }

        public async Task DeleteEmployeeAsync(int id)
        {
            var employee = await db.Employees.FindAsync(id);
            var subordinates = db.Employees.Where(e => e.ChiefId == id).ToList();
            subordinates.ForEach(e => e.ChiefId = null);                

            db.Employees.Remove(employee);
            await SaveAsync();
        }


        public async Task<Employee> GetEmployeeAsync(int id)
        {
            return await db.Employees.FindAsync(id);
        }

        public IQueryable<Employee> GetEmployees()
        {
            return db.Employees.AsQueryable();
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public async Task UpdateEmployeeAsync(Employee item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
            await SaveAsync();
        }

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
